package lab5.facci.yandrilopez.sms4a;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText editTextNumero,editTextMensaje;
    Button buttonEnviar;

    private final int REQUEST_PERMISION_SEND_SMS = 101;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTextMensaje = (EditText)findViewById(R.id.editTextMensaje);
        editTextNumero = (EditText)findViewById(R.id.editTextNumero);
        buttonEnviar = (Button)findViewById(R.id.buttonEnviar);

        buttonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Preguntae si tiene permiso para enviar
                //Sino solicitar permiso

                if (tienePermisoMensaje()){
                    //Enviar el mensaje
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    //activites extranjeras "tomar la funcionalidad la mensajeria de texto"
                    PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),0,intent,0);
                    //
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(editTextNumero.getText().toString(),null,
                            editTextMensaje.getText().toString(),pi,null);
                    Toast.makeText(getApplicationContext(),"SMS Sent",Toast.LENGTH_LONG).show();
                }else{
                    ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.SEND_SMS},
                            REQUEST_PERMISION_SEND_SMS);
                }

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }


    private boolean tienePermisoMensaje(){

        return ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                == PackageManager.PERMISSION_GRANTED;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
